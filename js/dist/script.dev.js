"use strict";

$(document).ready(function () {
  /**** Menu principal ****/

  /*Gestion menu desktop*/
  $('.link-menu').each(function () {
    $(this).click(function () {
      $(this).toggleClass('active');
      $(this).find('.ss-menu').css('z-index', '1');
      $(this).find('.ss-mn-un').css('z-index', '1');
    });
  });
  $('.ss-menu').find('li').click(function () {
    $(this).toggleClass('ss-link-active');
    $(this).closest('.link-menu').toggleClass('active');
  });
  /*Gestion menu mobile*/

  $('.toggle-btn').click(function () {
    $('#side-bar').toggleClass('active');
    $('body').toggleClass('animated apply-mw-small');
  });
  /****** Gestion Formulaire ******/

  $('.form-question').submit(function (evt) {
    evt.preventDefault();
    /*Gestion erreurs: Form Failed*/

    var nom = $('#input-nom').val();
    var email = $('#input-email').val();
    var textarea = $('#input-textarea').val();
    var empty = "";
    var errors = 0;
    /***** Regex *****/

    /* Regx chiffres */

    var intRegex = /^\d+$/;
    var floatRegex = /^((\d+(\.\d *)?)|((\d*\.)?\d+))$/;
    /* Regx char*/

    var characterRegex = /^\s*[~!@#$%^&*\s]+\s*$/;
    var atRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9])+$/;
    /*Test nom*/

    if (nom == empty || intRegex.test(nom) || floatRegex.test(nom) || characterRegex.test(nom)) {
      $('#input-nom').val("");
      $('#nomErrorMsg').html('<p>Les lettres anonymes, c\'est mal !</p>');
      $('#input-nom').click(function () {
        $('#nomErrorMsg').html('');
      });
      errors += 1;
    } else {
      $('#nomErrorMsg').css('display', 'none');
    }
    /*Test email*/


    if (!atRegex.test(email)) {
      $('#input-email').val("");
      $('#emailErrorMsg').html('<p>Et comment on fait pour vous spammer ?</p>');
      $('#input-email').click(function () {
        $('#emailErrorMsg').html('');
      });
      errors += 1;
    } else {
      $('#emailErrorMsg').css('display', 'none');
    }
    /*Test textarea*/


    if (textarea == empty) {
      $('#textareaErrorMsg').html('<p>Non mais allô quoi, tu nous écris et tu nous écris rien ?</p>');
      $('#input-textarea').click(function () {
        $('#textareaErrorMsg').html('');
      });
      errors += 1;
    } else {
      $('#textareaErrorMsg').css('display', 'none');
    }
    /**If no errors than submit**/


    if (errors == 0) {
      $('.modal-success').css('display', 'block');
      $('body').addClass('modal-sent');
      $('.btn-modal').click(function () {
        $('body').removeClass('modal-sent');
        $('.modal-success').css('display', 'none');
        $('#nomErrorMsg').html('');
        $('#emailErrorMsg').html('');
        $('#textareaErrorMsg').html('');
      });
    }
  });
});
/*Fin doc ready*/